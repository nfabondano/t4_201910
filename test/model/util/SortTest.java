package model.util;

import static org.junit.Assert.*;

import java.util.Comparator;
import java.util.concurrent.ThreadLocalRandom;

import org.junit.Before;
import org.junit.Test;

import model.util.Sort;

public class SortTest {

	// Muestra de datos a ordenar
	private Comparable[] datos;
	
	/**
	 * M�todo que se ejecuta y permite la inicializaci�n de los datos
	 * @throws Exception si no es posible ubicar espacio para el arreglo
	 */
	@Before
	public void setUp() throws Exception{
		datos = new Comparable[10000];
		for(int i = 0; i < 10000; i++) {
			int randomNum = ThreadLocalRandom.current().nextInt(1, 10000 + 1);
			datos[i] = randomNum;
		}
	}

	/**
	 * M�todo que prueba el correcto funcionamiento de MergeSort
	 */
	@Test
	public void MergeSortTest() {
		Sort.ordenarMergeSort(datos);
		for(int i = 1; i < 100; i++) {
			assertTrue("la lista no esta bien ordenada",datos[i-1].compareTo(datos[i])<=0);
		}
	}
	
	/**
	 * M�todo que prueba el correcto funcionamiento de ShellSort
	 */
	@Test
	public void ShellSortTest() {
		Sort.ordenarShellSort(datos);
		for(int i = 1; i < 100; i++) {
			assertTrue("la lista no esta bien ordenada",datos[i-1].compareTo(datos[i])<=0);
		}
	}
	
	/**
	 * M�todo que prueba el correcto funcionamiento de QuickSort
	 */
	@Test
	public void QuickSortTest() {
		Sort.ordenarQuickSort(datos);
		for(int i = 1; i < 100; i++) {
			assertTrue("la lista no esta bien ordenada",datos[i-1].compareTo(datos[i])<=0);
		}
		Sort.ordenarQuickSort(datos);
		for(int i = 1; i < 100; i++) {
			assertTrue("la lista no esta bien ordenada",datos[i-1].compareTo(datos[i])<=0);
		}
	}
}
