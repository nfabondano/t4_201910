package model.vo;

/**
 * Representation of a MovingViolation object
 */
public class VOMovingViolation implements Comparable<VOMovingViolation> {

	/**
	 * ObjectID del VOMovingViolations.
	 */
	private int objectID;

	/**
	 * Row del VOMovingViolations.
	 */
	private String ROW;

	/**
	 * Location del VOMovingViolations.
	 */
	private String location;

	/**
	 * AdressID del VOMovingViolations.
	 */
	private String adressID;

	/**
	 * StreetSegid del VOMovingViolations.
	 */
	private String streetSegid;

	/**
	 * XCOORD del VOMovingViolations.
	 */
	private double XCOORD;

	/**
	 * YCOORD del VOMovingViolations.
	 */
	private double YCOORD;

	/**
	 * TicketType del VOMovingViolations.
	 */
	private String ticketType;

	/**
	 * FineAMT del VOMovingViolations.
	 */
	private int fineAMT;

	/**
	 * TotalPaid del VOMovingViolations.
	 */
	private int totalPaid;

	/**
	 * Penalty1 del VOMovingViolations.
	 */
	private int penalty1;

	/**
	 * Penalty2 del VOMovingViolations.
	 */
	private int penalty2;

	/**
	 * AccidentIndicator del VOMovingViolations.
	 */
	private String accidentIndicator;

	/**
	 * TicketIssueDate del VOMovingViolations.
	 */
	private String ticketIssueDate;

	/**
	 * ViolationCode del VOMovingViolations.
	 */
	private String violationCode;

	/**
	 * ViolationDescription del VOMovingViolations.
	 */
	private String violationDescription;

	/**
	 * ROWID del VOMovingViolations.
	 */
	private String ROWID;


	/**
	 * Metodo constructor
	 */
	public VOMovingViolation(  )
	{
		// TODO Implementar
	}	
	
	/**
	 * Constructor del VOMovingViolations.
	 * @param pLine Linea para construir el VOMovingVIolations
	 */
	public VOMovingViolation( String[] pLine ) {
		objectID = Integer.parseInt(pLine[0]);
		ROW = pLine[1];
		location = pLine[2];
		adressID = pLine[3];	
		streetSegid = pLine[4];
		XCOORD = Double.parseDouble(pLine[5]);
		YCOORD = Double.parseDouble(pLine[6]);
		ticketType = pLine[7];
		fineAMT = Integer.parseInt(pLine[8]);
		totalPaid = Integer.parseInt(pLine[9]);
		penalty1 = Integer.parseInt(pLine[10]);
		try{
			penalty2 = Integer.parseInt(pLine[11]);
		}
		catch(Exception e){
			penalty2 = 0;
		}
		accidentIndicator = pLine[12];
		ticketIssueDate = pLine[13];
		violationCode = pLine[14];
		violationDescription = pLine[15];
		ROWID = pLine[16];
	}

	/**
	 * @return id - Identificador único de la infracción
	 */
	public int objectID() {
		return objectID;
	}	

	/**
	 * @return Row del VOMovingViolations.
	 */
	public String getROW() {
		return ROW;
	}


	/**
	 * @return location - Dirección en formato de texto.
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @return AdressID del VOMovingViolations.
	 */
	public String getAdressID(){ 
		return adressID;
	}

	/**
	 * @return StreetSgid del VOMovingViolations.
	 */
	public String getStreetSgid() {
		return streetSegid;
	}

	/**
	 * @return XCOORD del VOMovingViolations.
	 */
	public double getXCOORD(){
		return XCOORD;
	}

	/**
	 * @return YCOORD del VOMovingViolations.
	 */
	public double getYCOORD(){ 
		return YCOORD;
	}

	/**
	 * @return TicketType del VOMovingViolations.
	 */
	public String getTicketType(){ 
		return ticketType;
	}

	/**
	 * @return FineAMT del VOMovingViolations.
	 */
	public int getfineAMT(){
		return fineAMT;
	}

	/**
	 * @return totalPaid - Cuanto dinero efectivamente pagó el que recibió la infracción en USD.
	 */
	public int getTotalPaid() {
		return totalPaid;
	}

	/**
	 * @return Penalty1 del VOMovingViolations.
	 */
	public int penalty1(){ 
		return penalty1;
	}

	/**
	 * @return Penalty2 del VOMovingViolations.
	 */
	public int penalty2(){ 
		return penalty2;
	}

	/**
	 * @return accidentIndicator - Si hubo un accidente o no.
	 */
	public String getAccidentIndicator() {
		return accidentIndicator;
	}

	/**
	 * @return date - Fecha cuando se puso la infracción .
	 */
	public String getTicketIssueDate() {
		return ticketIssueDate;
	}

	/**
	 * @return violationCode - Devuelve el c�digo de violaci�n.
	 */
	public String getViolationCode() {
		return violationCode;
	}

	/**
	 * @return description - Descripción textual de la infracción.
	 */
	public String  getViolationDescription() {
		return violationDescription;
	}

	/**
	 * @return ROWID del VOMovingViolations.
	 */
	public String getROWID(){ 
		return ROWID;
	}
	
	@Override
	// POR FECHA ejemplo fecha 2018-02-02T11:07:00.000Z,
	public int compareTo(VOMovingViolation pComparar) {
		String[] date1 = ticketIssueDate.split("T");
		String[] fecha1 = date1[0].split("-");
		String[] hora1 = date1[1].split(":");
		String[] date2 = pComparar.getTicketIssueDate().split("T");
		String[] fecha2 = date2[0].split("-");
		String[] hora2 = date2[1].split(":");
		try {
			int a�o1 = Integer.parseInt(fecha1[0]);
			int a�o2 = Integer.parseInt(fecha2[0]);
			int mes1 = Integer.parseInt(fecha1[1]);
			int mes2 = Integer.parseInt(fecha2[1]);
			int dia1 = Integer.parseInt(fecha1[2]);
			int dia2 = Integer.parseInt(fecha2[2]);
			int hor1 = Integer.parseInt(hora1[0]);
			int hor2 = Integer.parseInt(hora2[0]);
			int min1 = Integer.parseInt(hora1[1]);
			int min2 = Integer.parseInt(hora2[1]);
			double seg1 = Double.parseDouble(hora1[2].split("Z")[0]);
			double seg2 = Double.parseDouble(hora1[2].split("Z")[0]);
			
			if(a�o1 > a�o2) 
				return 1;
			else if(a�o1 < a�o2)
				return -1;
			else if(mes1 > mes2)
				return 1;
			else if(mes1 < mes2)
				return -1;
			else if(dia1 > dia2)
				return 1;
			else if(dia1 < dia2)
				return -1;
			else if(hor1 > hor2)
				return 1;
			else if(hor1 < hor2)
				return -1;
			else if(min1 > min2)
				return 1;
			else if(min1 < min2)
				return -1;
			else if(seg1 > seg2)
				return 1;
			else if(seg1 < seg2)
				return -1;
			else if(objectID > pComparar.objectID())
				return 1;
			else if(objectID < pComparar.objectID())
				return -1;
			else 
				return 0;
				
		}
		catch (Exception e) {
			return -2;
		}
	}
	
	public String toString(){
		return objectID + "-" + ticketIssueDate;
	}
}
