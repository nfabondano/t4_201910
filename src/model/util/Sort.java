package model.util;
import java.util.Random;

public class Sort {

	private static Comparable[] aux;
	
	/**
	 * Ordenar datos aplicando el algoritmo ShellSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarShellSort( Comparable[ ] datos ) {
		int n = datos.length;
		int h = 1;
		while( h<n/3)
			h = 3*h +1;
		while(h>=1) {
			for (int i = h; i < n; i++) 
				for (int j = i; j >= h && less(datos[j], datos[j-h]); j-=h) 
					exchange(datos, j, j-h);
			h = h/3;
		}
	}

	/**
	 * M�todo que se encarga de unir dos partes ordenas parcialmente de un arreglo
	 * @param datos el conjunto de datos que se va a ordenar. datos != null.
	 * @param lo el punto desde el cual se ordena lo > 0 && lo < datos.length
	 * @param hi el punto hasta el cual se ordena hi < datos.length && hi > 0
	 * post: los pedazos del arreglo definidos en el rango han quedado ordenados
	 */
	private static void Merge(Comparable[] datos, int lo, int hi, int mid){
		for(int i = lo; i <= hi; i++){
			aux[i] = datos[i];
		}

		int first = lo;
		int second = mid + 1;
		for(int i = lo; i <= hi; i++){
			if(first > mid ) datos[i] = aux[second++];
			else if(second > hi) datos[i] = aux[first++];
			else if(less(aux[second],aux[first])) datos[i] = aux[second++];
			else datos[i] = aux[first++];
		}
	}

	/**
	 * 
	 * @param datos datos el conjunto de datos que se va a ordenar. datos != null.
	 * @param lo el punto desde el cual se ordena lo > 0 && lo < datos.length
	 * @param hi el punto hasta el cual se ordena hi < datos.length && hi > 0
	 */
	private static void MergeSort(Comparable[] datos, int lo, int hi){
		if(lo >= hi) return;
		int mid = lo + (hi - lo)/2;
		MergeSort(datos,lo, mid);
		MergeSort(datos, mid +1, hi);
		Merge(datos,lo, hi,mid);
	}

	/**
	 * Ordenar datos aplicando el algoritmo MergeSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarMergeSort( Comparable[ ] datos ) {
		aux = new Comparable[datos.length];
		MergeSort(datos,0, datos.length -1);
	}
	
	/**
	 * M�todo que se encarga de generar la partici�n correcta y intercambia los elementos 
	 * de tal manera que en la izquierda queden elementos menores a dato y la derecha mayores 
	 * @param datos el conjunto que se va ordenar seg�n lo anterior enunciado. datos != null
	 * @param lo el indice desde el cual se busca partir el arreglo. lo > 0 && lo < datos.length
	 * @param hi el indide hasta el cual se busca partir el arreglo. hi < datos.length && hi > 0
	 * @return el indice que cumple que hacia la izquierda todos son menores o iguales y a la derecha son mayores o iguales
	 */
	private static int partition(Comparable[] datos, int lo, int hi){
		int i = lo, j = hi + 1;
		Comparable dato = datos[lo];
		while(i < j){
			while(less(datos[++i],dato)) if(i == hi) break;
			while(less(dato,datos[--j])) if(j == 0) break;
			if(i >= j)
				break;
			exchange(datos,i,j);
		}
		exchange(datos,lo,j);
		return j;
	}
	
	/**
	 * M�todo privado que se encarga de ordenar el arreglo
	 * @param datos el conjunto de datos que se va a ordenar
	 * @param lo el punto desde el cual se ordena lo > 0 && lo < datos.length
	 * @param hi el punto hasta el cual se ordena hi < datos.length && hi > 0
	 * post se ha ordenado el arreglo entre la posici�n lo y hi
	 */
	private static void QuickSort(Comparable[] datos, int lo, int hi){
		if(lo >= hi) return;
		//System.out.println("valores de lo y hi"+ lo + " " +hi );
		int j = partition(datos,lo, hi);
		QuickSort(datos,lo, j -1);
		QuickSort(datos, j+ 1, hi);
		
	}
	/**
	 * Ordenar datos aplicando el algoritmo QuickSort
	 * @param datos - conjunto de datos a ordenar (inicio) y conjunto de datos ordenados (final)
	 */
	public static void ordenarQuickSort( Comparable[ ] datos ) {
		shuffle(datos);
		QuickSort(datos,0,datos.length -1);
	}

	/**
	 * Comparar 2 objetos usando la comparacion "natural" de su clase
	 * @param v primer objeto de comparacion
	 * @param w segundo objeto de comparacion
	 * @return true si v es menor que w usando el metodo compareTo. false en caso contrario.
	 */
	private static boolean less(Comparable v, Comparable w)
	{
		// TODO implementar
		return v.compareTo(w) < 0;
	}

	/**
	 * Intercambiar los datos de las posicion i y j
	 * @param datos contenedor de datos
	 * @param i posicion del 1er elemento a intercambiar
	 * @param j posicion del 2o elemento a intercambiar
	 */
	private static void exchange( Comparable[] datos, int i, int j)
	{
		Comparable temp = datos[i];
		datos[i] = datos[j];
		datos[j] = temp;
	}

	/**
	 * M�todo que cambia la posici�n de los elementos en el arreglo pasado por parametro
	 * @param arreglo un arreglo comparable
	 * post: se ha cambiado el orden del arreglo
	 */
	private static void shuffle(Comparable[] arreglo) {
		Random random = null;
		if (random == null) random = new Random();
		int count = arreglo.length;
		for (int i = count; i > 1; i--) {
			exchange(arreglo, i - 1, random.nextInt(i));
		}
	}
}
